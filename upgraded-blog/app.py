import requests as requests
from flask import Flask, render_template, request
import smtplib

app = Flask(__name__)
app.config['ENV'] = 'development'

blogs_list = requests.get(url='https://api.npoint.io/2ace90d9613974a0b185').json()

def get_blog_by_id(_id: int):
    for blog in blogs_list:
        if blog['id'] == _id:
            return blog

def send_mail(name, email, phone, msg):
    with smtplib.SMTP("smtp.gmail.com", port=587) as connection:
        connection.starttls()
        connection.login(user="antwanus1337@gmail.com", password="geheimpje")
        connection.sendmail(
            from_addr="antwanus1337@gmail.com",
            to_addrs="antwanus1337@gmail.com",
            msg=f"Subject:{name} has sent a msg!\n\n"
                f"Message: {msg}\n"
                f"Mail me: {email}"
                f"Call me: {phone}"
        )


@app.route('/')
def get_all_posts():
    return render_template('index.html', blogs=blogs_list)

@app.route('/post/<int:blog_id>')
def post(blog_id):
    return render_template('post.html', blog=get_blog_by_id(blog_id))

@app.route('/form-entry', methods=['POST', 'GET'])
def receive_data():
    if request.method == 'GET':
        return render_template('contact.html', msg_sent=False)
    else:
        name = request.form['name']
        email = request.form['email']
        phone = request.form['phone']
        msg = request.form['message']
        send_mail(name, email, phone, msg)
        return render_template('contact.html', msg_sent=True)

@app.route('/about')
def about():
    return render_template('about.html')

@app.route('/contact')
def contact():
    return render_template('contact.html', msg_sent=False)


if __name__ == '__main__':
    app.run()
